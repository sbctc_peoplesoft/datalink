/*
-------------------
DESCRIPTION
-------------------

Created By: David Mielcarek, Lower Columbia College, 20220201
Modified By: David Mielcarek, Lower Columbia College, 20220201

Description: search all tables/columns in a database.  Built to be ran on the same server as the source and report databases.

-------------------
FILTERED LINES
-------------------
Line: n/a
Note: see PARAMETERS and examples below

Update 20220201: Added Filter Out Schema.Tables.  This added the line:
AND TABLE_SCHEMA+'.'+TABLE_NAME NOT IN (SELECT lccSSchemaTable FROM dbo.lccTblFilterOutSchemaTables)
To use, create a table called dbo.lccTblFilterOutSchemaTables with:
CREATE TABLE [dbo].[lccTblFilterOutSchemaTables]([lccSSchemaTable] [varchar](100) NOT NULL) ON [PRIMARY]
Then insert the scheam.tables you want skipped with:
example: INSERT INTO dbo.lccTblFilterOutSchemaTables (lccSSchemaTable) VALUES ('SYSADM_CS.PS_OPRDEFN');

------- PARAMETERS -------

@lcParamSDatabase: what database to search
,@lccParamSFilter: what to search for
,@lccParamSDataTypes: what data type columns to search in [comma delimited], ex: 'char,varchar,nchar,nvarchar', 'int'
,@lccParamSReportDatabase: what database for the matched report table
,@lccParamSReportTableSchema: what schema for the matched report table
,@lccParamSReportTable: what table will receive the matched report records
,@lccParamBDisplayResults: '0' - will only say when search completed, '1' will print results to the screen
,@lccParamBLIKE: '0' - filter must match the value exactly, '1' fitler can be within the value

------- USAGE SYNTAX -------
EXEC	[dbo].[lccFSearchAllTablesColumns]
@lcParamSDatabase = N'...databaseToSearch...'
,@lccParamSFilter = N'...filter....'
,@lccParamSDataTypes = N'...data type,data type...'
,@lccParamSReportDatabase = '...report database'
,@lccParamSReportTableSchema = '...report schema...'
,@lccParamSReportTable = '...report table...'
,@lccParamBDisplayResults = 1
,@lccParamBLIKE = 1

------- EXAMPLE: search for 'jsmith' across text based column data types, any value containing [LIKE] -------
EXEC	[dbo].[lccFSearchAllTablesColumns]
@lcParamSDatabase = N'ourDatabase'
,@lccParamSFilter = N'jsmith'
,@lccParamSDataTypes = N'char,varchar,nchar,nvarchar'
,@lccParamSReportDatabase = 'reportDatabase'
,@lccParamSReportTableSchema = 'dbo'
,@lccParamSReportTable = 'reportTable'
,@lccParamBDisplayResults = 1
,@lccParamBLIKE = 1

------- EXAMPLE: search for '1234' across int columns, any value matching -------
EXEC	[dbo].[lccFSearchAllTablesColumns]
@lcParamSDatabase = N'ourDatabase'
,@lccParamSFilter = N'1234'
,@lccParamSDataTypes = N'int'
,@lccParamSReportDatabase = 'reportDatabase'
,@lccParamSReportTableSchema = 'dbo'
,@lccParamSReportTable = 'reportTable'
,@lccParamBDisplayResults = 1
,@lccParamBLIKE = 0

-------------------
CODE
-------------------
*/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		David Mielcarek
-- Create date: 20210319
-- Description:	Search all Tables and Columns
-- =============================================

CREATE OR ALTER PROCEDURE [dbo].[lccFSearchAllTablesColumns]
@lcParamSDatabase varchar(200)
,@lccParamSFilter varchar(200)
,@lccParamSDataTypes varchar(200)
,@lccParamSReportDatabase varchar(200)
,@lccParamSReportTableSchema varchar(200)
,@lccParamSReportTable varchar(200)
,@lccParamBDisplayResults bit
,@lccParamBLike bit
AS
BEGIN

DECLARE @lccSDatabaseLog nvarchar(200);
DECLARE @lccSDatabase nvarchar(200);
DECLARE @lccSCheckTable nvarchar(200);
DECLARE @lccSCheckColumn nvarchar(200)
DECLARE @lccSSearchValue nvarchar(200);
DECLARE @lccSFinalSearchValue nvarchar(200);
DECLARE @lccSCommand varchar(2000);
DECLARE @lccALDataTypes TABLE ([Name] [nvarchar] (100));
DECLARE @lccSDataTypes varchar(200);
DECLARE @lccSDataTypesSplitName nvarchar(100);
DECLARE @lccSColumnDataType nvarchar(100);
DECLARE @lccIDataTypesSplitPos INT;
DECLARE @lccSLike nvarchar(10);

SET  @lccSCheckTable = '';

SET @lccSDatabaseLog = 'lccTblFullDatabaseAuditValue'

IF LEN(@lccParamSReportDatabase)>0
SET @lccParamSReportDatabase=@lccParamSReportDatabase+'.';
IF LEN(@lccParamSReportTableSchema)>0
SET @lccParamSReportTableSchema=@lccParamSReportTableSchema+'.';

IF LEN(@lccParamSReportTable)>0
SET @lccSDatabaseLog=@lccParamSReportDatabase+@lccParamSReportTableSchema+@lccParamSReportTable;

SET @lccSDatabase=@lcParamSDatabase;
SET @lccSSearchValue = @lccParamSFilter;
IF (@lccParamBLike=0)
BEGIN
SET @lccSFinalSearchValue = QUOTENAME(@lccSSearchValue,'''');
SET @lccSLike='=';
END
ELSE
BEGIN
SET @lccSFinalSearchValue = QUOTENAME('%' + @lccSSearchValue + '%','''');
SET @lccSLike='LIKE';
END


SET @lccSDataTypes= 'char,varchar,nchar,nvarchar';

IF LEN(@lccParamSDataTypes)>0
SET @lccSDataTypes=@lccParamSDataTypes;

WHILE CHARINDEX(',', @lccSDataTypes) > 0
 BEGIN
  SELECT @lccIDataTypesSplitPos  = CHARINDEX(',', @lccSDataTypes);
  SELECT @lccSDataTypesSplitName = SUBSTRING(@lccSDataTypes, 1, @lccIDataTypesSplitPos-1);

  INSERT INTO @lccALDataTypes SELECT @lccSDataTypesSplitName;
  SELECT @lccSDataTypes = SUBSTRING(@lccSDataTypes, @lccIDataTypesSplitPos+1, LEN(@lccSDataTypes)-@lccIDataTypesSplitPos);
 END
INSERT INTO @lccALDataTypes SELECT @lccSDataTypes;

SET @lccSCommand = 'DECLARE @lccSCommand varchar(2000); IF EXISTS (SELECT * FROM '+@lccParamSReportDatabase+'dbo.sysobjects WHERE id = object_id('''+@lccParamSReportTable+''') and OBJECTPROPERTY(id, N''IsUserTable'') = 1)'+
' BEGIN'+
' SET @lccSCommand = ''DROP TABLE '+@lccSDatabaseLog+';'''+
' EXEC(@lccSCommand);'+
' END'
EXEC(@lccSCommand);


SET @lccSCommand = 'DECLARE @lccSCommand varchar(2000); IF NOT EXISTS (SELECT * FROM '+@lccParamSReportDatabase+'sys.objects WHERE name='''+@lccParamSReportTable+''' and type=''U'')'+
' BEGIN'+
' SET @lccSCommand = ''CREATE TABLE '+@lccSDatabaseLog+' ('+
	' [lccIRecId] [int] IDENTITY (1, 1) NOT NULL'+
	',[lccSDatabase] [varchar] (100) NULL'+
	',[lccSTable] [varchar] (100) NULL'+
	',[lccSColumn] [varchar] (100) NULL'+
	',[lccSValue] [varchar] (1000) NULL'+
    ') ON [PRIMARY]'+
    ''' END'+
' EXEC(@lccSCommand);'
EXEC(@lccSCommand);
SET @lccSCommand = 'TRUNCATE TABLE '+@lccSDatabaseLog;
EXEC(@lccSCommand);

WHILE @lccSCheckTable IS NOT NULL
BEGIN
    SET @lccSCheckColumn = '';
    SET @lccSCheckTable = 
    (
        SELECT MIN(QUOTENAME(TABLE_SCHEMA) + '.' + QUOTENAME(TABLE_NAME))
        FROM    INFORMATION_SCHEMA.TABLES
        WHERE       TABLE_TYPE = 'BASE TABLE'
            AND QUOTENAME(TABLE_SCHEMA) + '.' + QUOTENAME(TABLE_NAME) > @lccSCheckTable
            AND OBJECTPROPERTY(
                    OBJECT_ID(
                        QUOTENAME(TABLE_SCHEMA) + '.' + QUOTENAME(TABLE_NAME)
                         ), 'IsMSShipped'
                           ) = 0
            AND TABLE_SCHEMA+'.'+TABLE_NAME NOT IN (SELECT lccSSchemaTable FROM dbo.lccTblFilterOutSchemaTables)
    )
    WHILE (@lccSCheckTable IS NOT NULL) AND (@lccSCheckColumn IS NOT NULL)
    BEGIN
        SET @lccSCheckColumn =
        (
            SELECT MIN(QUOTENAME(COLUMN_NAME))
            FROM    INFORMATION_SCHEMA.COLUMNS
            WHERE       TABLE_SCHEMA    = PARSENAME(@lccSCheckTable, 2)
                AND TABLE_NAME  = PARSENAME(@lccSCheckTable, 1)
                AND DATA_TYPE IN (SELECT Name FROM @lccALDataTypes)
                AND QUOTENAME(COLUMN_NAME) > @lccSCheckColumn
        )
        SET @lccSColumnDataType =
        (
            SELECT DATA_TYPE
            FROM    INFORMATION_SCHEMA.COLUMNS
            WHERE       TABLE_SCHEMA    = PARSENAME(@lccSCheckTable, 2)
                AND TABLE_NAME  = PARSENAME(@lccSCheckTable, 1)
                AND DATA_TYPE IN (SELECT Name FROM @lccALDataTypes)
                AND QUOTENAME(COLUMN_NAME) = @lccSCheckColumn
        )
		

        IF @lccSCheckColumn IS NOT NULL
        BEGIN

		SET @lccSCommand= 'DECLARE @lccBFound int;'+
		' IF EXISTS (SELECT '''+
		@lccSDatabase+'.'+@lccSCheckTable+'.'+@lccSCheckColumn+''' AS TableColumn'+
		', LEFT('+@lccSCheckColumn+', 1000) AS ColumnValue'+
		' FROM '+@lccSCheckTable+
		' WHERE '+@lccSCheckColumn+' '+@lccSLike+' '+@lccSFinalSearchValue+')'+
		' INSERT INTO '+@lccSDatabaseLog+' (lccSDatabase,lccSTable,lccSColumn,lccSValue)'+
		' SELECT'+
		''''+@lccSDatabase+''''+
		','''+@lccSCheckTable+''''+
		','''+@lccSCheckColumn+''''+
		',LEFT('+@lccSCheckColumn+', 3630) AS lccSValue'+
		' FROM '+@lccSCheckTable+''+
		' WHERE '+@lccSCheckColumn+' '+@lccSLike+' '+@lccSFinalSearchValue+
		' ELSE BEGIN SET @lccBFound=1; END';
		EXEC (@lccSCommand);
        END
    END 
END
IF (@lccParamBDisplayResults=1)
BEGIN
SET @lccSCommand='SELECT * FROM '+@lccSDatabaseLog;
EXEC(@lccSCommand);
END
ELSE
SELECT 'Search completed';
END
GO