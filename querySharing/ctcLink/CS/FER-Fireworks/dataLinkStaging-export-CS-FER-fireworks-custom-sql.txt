
------------------------------------------
/*
NOTES:
...1... : database used to store adhoc tables.
lccFConvertTermToFriendly : function to convert Term code into friendly format, ex: Spring 2022
*/
------------------------------------------

SELECT
T0.[Person Unique Key] AS 'Person Unique Key'
,'ctcLink Last Sign On' AS 'Custom Category'
,'Yes' AS 'Custom Value'
,CONVERT(VARCHAR,T1.LastSignOnDTTM,101) AS 'Custom Date'
,'' AS 'Custom Comments'
,T0.[First Name] AS 'First Name' -- Primary
,T0.[Last Name] AS 'Last Name' -- Primary
,T0.Email AS 'Email'
,T0.[Email Type] AS 'Email Type'
,T2.Major1 AS 'Student Type'
,'Application Complete' AS 'Student Status'
,[...1...].dbo.lccFConvertTermToFriendly(T2.[Entry Term]) AS 'Entry Term'
,CONVERT(VARCHAR,T2.ADM_APPL_DT,101) AS 'Student Status Date'
FROM dbo.lccAdhoc_CS_FER_Fireworks_contactNames T0
LEFT JOIN dbo.lccAdhoc_CS_LastSignOn T1
ON T1.OPRID=T0.[Person Unique Key]
LEFT JOIN [...1...].[dbo].[FER_Fireworks_ApplicationGeneral] T2
ON T2.[Person Unique Key]=T0.[Person Unique Key]
AND T2.ADM_APPL_DT = (SELECT MAX(T2_1.[ADM_APPL_DT]) FROM [...1...].[dbo].[FER_Fireworks_ApplicationGeneral] T2_1 WHERE T2_1.[Person Unique Key]=T2.[Person Unique Key])
AND T2.[ADM_APPL_NBR] = (SELECT MAX(T2_1.[ADM_APPL_NBR]) FROM [...1...].[dbo].[FER_Fireworks_ApplicationGeneral] T2_1 WHERE T2_1.[Person Unique Key]=T2.[Person Unique Key] AND T2_1.ADM_APPL_DT=T2.ADM_APPL_DT)
WHERE T1.LastSignOnDTTM IS NOT NULL
/*
-- No longer using this join for Running Start, but left in for reference
UNION ALL

SELECT
T0.[Person Unique Key] AS 'Person Unique Key'
,'Running Start' AS 'Custom Category'
,CASE WHEN T1.EFFDT IS NULL THEN 'No' ELSE 'Yes' END AS 'Custom Value'
,COALESCE(T1.EFFDT,CONVERT(VARCHAR,GetDate(),101)) AS 'Custom Date'
,'' AS 'Custom Comments'
,T0.[First Name] AS 'First Name' -- Primary
,T0.[Last Name] AS 'Last Name' -- Primary
,T0.Email AS 'Email'
,T0.[Email Type] AS 'Email Type'
,T2.Major1 AS 'Student Type'
,'Application Complete' AS 'Student Status'
,T2.[Entry Term] AS 'Entry Term'
,CONVERT(VARCHAR,T2.ADM_APPL_DT,101) AS 'Student Status Date'
FROM dbo.lccAdhoc_CS_FER_Fireworks_contactNames T0
LEFT JOIN SYSADM_CS.PS_STDNT_GRPS_HIST T1 -- EFFDT, EFF_STATUS
ON T1.EMPLID=T0.[Person Unique Key]
AND T1.STDNT_GROUP IN ('SRSL','SRSR')
AND T1.EFF_STATUS='A'
AND T1.EFFDT = (
SELECT MAX(T1_1.EFFDT) FROM SYSADM_CS.PS_STDNT_GRPS_HIST T1_1 WHERE T1_1.EMPLID=T1.EMPLID
)
LEFT JOIN [...1...].[dbo].[FER_Fireworks_ApplicationGeneral] T2
ON T2.[Person Unique Key]=T0.[Person Unique Key]
AND T2.ADM_APPL_DT = (SELECT MAX(T2_1.[ADM_APPL_DT]) FROM [...1...].[dbo].[FER_Fireworks_ApplicationGeneral] T2_1 WHERE T2_1.[Person Unique Key]=T2.[Person Unique Key])
AND T2.[ADM_APPL_NBR] = (SELECT MAX(T2_1.[ADM_APPL_NBR]) FROM [...1...].[dbo].[FER_Fireworks_ApplicationGeneral] T2_1 WHERE T2_1.[Person Unique Key]=T2.[Person Unique Key] AND T2_1.ADM_APPL_DT=T2.ADM_APPL_DT)
*/

ORDER BY [Person Unique Key];
