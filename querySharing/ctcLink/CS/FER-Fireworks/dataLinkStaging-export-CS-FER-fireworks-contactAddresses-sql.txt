
------------------------------------------
/*
NOTES:
...1... : database used to store adhoc tables.
lccFConvertTermToFriendly : function to convert Term code into friendly format, ex: Spring 2022
*/
------------------------------------------

SELECT
T0.[Person Unique Key] AS 'Person Unique Key'
,'1' AS 'Primary'
,'1' AS 'Active'
,COALESCE(T1.ADDRESS_TYPE,T3.ADDRESS_TYPE) AS 'Address Type'
,COALESCE(T1.ADDRESS1,T3.ADDRESS1) AS 'Street1'
,COALESCE(T1.ADDRESS2,T3.ADDRESS2) AS 'Street2'
,COALESCE(T1.ADDRESS3,T3.ADDRESS3) AS 'Street3'
,COALESCE(T1.ADDRESS4,T3.ADDRESS4) AS 'Street4'
,COALESCE(T1.CITY,T3.CITY) AS 'City'
,COALESCE(T1.STATE,T3.STATE) AS 'State / Province'
,COALESCE(T1.POSTAL,T3.POSTAL) AS 'Postal Code'
,COALESCE(T1.COUNTRY,T3.COUNTRY) AS 'Country'
,T0.[First Name] AS 'First Name' -- Primary
,T0.[Last Name] AS 'Last Name' -- Primary
,T0.Email AS 'Email'
,T0.[Email Type] AS 'Email Type'
,T2.Major1 AS 'Student Type'
,'Application Complete' AS 'Student Status'
,[...1...].dbo.lccFConvertTermToFriendly(T2.[Entry Term]) AS 'Entry Term'
,CONVERT(VARCHAR,T2.ADM_APPL_DT,101) AS 'Student Status Date'
FROM dbo.lccAdhoc_CS_FER_Fireworks_contactNames T0
LEFT JOIN SYSADM_CS.PS_ADDRESSES_GGVW T1 -- EFFDT, EFF_STATUS
ON T1.EMPLID = T0.[Person Unique Key] AND T1.EFF_STATUS='A' AND T1.ADDRESS_TYPE='HOME'
AND T1.EFFDT = (
SELECT MAX(T1_1.EFFDT) FROM SYSADM_CS.PS_ADDRESSES_GGVW T1_1 WHERE T1_1.EMPLID=T1.EMPLID AND T1_1.EFF_STATUS='A' AND T1_1.ADDRESS_TYPE='HOME'
)
LEFT JOIN SYSADM_CS.PS_ADDRESSES_GGVW T3 -- EFFDT, EFF_STATUS
ON T3.EMPLID = T0.[Person Unique Key] AND T3.EFF_STATUS='A' AND T3.ADDRESS_TYPE!='HOME'
AND T1.EFFDT IS NULL
AND T3.EFFDT = (
SELECT MAX(T3_1.EFFDT) FROM SYSADM_CS.PS_ADDRESSES_GGVW T3_1 WHERE T3_1.EMPLID=T3.EMPLID AND T3_1.EFF_STATUS='A' AND T3_1.ADDRESS_TYPE!='HOME'
)
LEFT JOIN [...1...].[dbo].[FER_Fireworks_ApplicationGeneral] T2
ON T2.[Person Unique Key]=T0.[Person Unique Key]
AND T2.ADM_APPL_DT = (SELECT MAX(T2_1.[ADM_APPL_DT]) FROM [...1...].[dbo].[FER_Fireworks_ApplicationGeneral] T2_1 WHERE T2_1.[Person Unique Key]=T2.[Person Unique Key])
AND T2.[ADM_APPL_NBR] = (SELECT MAX(T2_1.[ADM_APPL_NBR]) FROM [...1...].[dbo].[FER_Fireworks_ApplicationGeneral] T2_1 WHERE T2_1.[Person Unique Key]=T2.[Person Unique Key] AND T2_1.ADM_APPL_DT=T2.ADM_APPL_DT)
WHERE T1.EFFDT IS NOT NULL
OR T3.EFFDT IS NOT NULL
ORDER BY T0.[Person Unique Key]