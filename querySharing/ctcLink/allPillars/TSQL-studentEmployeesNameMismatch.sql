/*
-------------------
DESCRIPTION
-------------------

Created By: David Mielcarek, Lower Columbia College, 20220310
Modified By: David Mielcarek, Lower Columbia College, 20220310

Compare students who are employees and the first or last names do not match.  Sync issue?

-------------------
FILTERED LINES
-------------------
n/a

-------------------
CODE
-------------------
*/
SELECT
T1.EMPLID
,T1.LAST_NAME
,T1.FIRST_NAME
,T2.LAST_NAME
,T2.FIRST_NAME
FROM SYSADM_CS.PS_PERSONAL_DATA_GGVW T1
LEFT JOIN SYSADM_HCM.PS_PERSONAL_DATA_GGVW T2
ON T2.EMPLID=T1.EMPLID
WHERE T2.LAST_NAME!=T1.LAST_NAME OR T2.FIRST_NAME!=T1.FIRST_NAME
ORDER BY T1.EMPLID