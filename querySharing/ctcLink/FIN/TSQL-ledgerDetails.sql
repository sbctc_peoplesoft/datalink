/*
-------------------
DESCRIPTION
-------------------

Created By: David Mielcarek, Lower Columbia College, 20210226
Modified By: David Mielcarek, Lower Columbia College, 20210226

Retrieve ledger details.

This query code was copied from the lccQueryReporter tool: https://services4.lowercolumbia.edu/demo/projectlid/LCCQueryReporter/

-------------------
FILTERED LINES
-------------------
Line: ...many lines...each contains "[lccColumnFilter"
Note: these filters are meant to be used with the lccQueryReporter tool.  To use as-is, please replace all filter lines with your filtering logic.

-------------------
CODE
-------------------
*/

SELECT
TOP 1000
[T14].[DESCR] as DEPT_DESCR
,[T21].[ACCOUNT]
,[T21].[FUND_CODE]
,[T21].[PRODUCT]
,[T21].[CLASS_FLD]
,[T21].[DEPTID]
,[T15].[DESCR] as ACCOUNT_DESCR
,[T1].[REQ_ID]
,[T1].[REQ_STATUS] -- A,C,O,P,X
,[T2].[SOURCE_STATUS]
,[T16].[PO_ID]
,[T2].[VENDOR_ID]
,[T8].[NAME1]
,[T8].[NAME2]
,[T9].[CONTACT_NAME]
,[T9].[EMAILID]
,[T9].[URL]
,[T2].[LINE_NBR]
,[T1].[REQ_NAME]
,[T2].[DESCR254_MIXED]
,[T2].[CATEGORY_ID]
,[T2].[QTY_REQ]
,[T2].[UNIT_MEASURE_STD]
,format([T2].[PRICE_REQ], 'N2') AS PRICE_REQ
,format([T16].[MERCHANDISE_AMT], 'N2') AS PO_MERCHANDISE_AMT
,format([T2].[MERCHANDISE_AMT], 'N2') AS MERCHANDISE_AMT
,[T16].[LINE_NBR]
,[T16].[QTY_PO]
,format([T16].[PC_DISTRIB_AMT], 'N2') AS PO_PC_DISTRIB_AMT
,format([T16].[SALETX_AMT], 'N2') AS PO_SALETX_AMT
,format([T16].[MONETARY_AMOUNT], 'N2') AS PO_MONETARY_AMOUNT
,'' AS POAdhocTotal
,[T16].[ATTN_TO]
,[T17].[PO_REF]
,[T18].[DESCR254_MIXED]
,convert(varchar(10),cast([T1].[REQ_DT] as DATE),101) AS REQ_DT
,convert(varchar(10),cast([T1].[ENTERED_DT] as DATE),101) AS ENTERED_DT
,convert(varchar(10),cast([T1].[APPROVAL_DT] as DATE),101) AS APPROVAL_DT
,convert(varchar(10),cast([T17].[ENTERED_DT] as DATE),101) AS PO_ENTERED_DT
,convert(varchar(10),cast([T17].[PO_DT] as DATE),101) AS PO_DT
,convert(varchar(10),cast([T17].[APPROVAL_DT] as DATE),101) AS PO_APPROVAL_DT
,convert(varchar(10),cast([T1].[ACCOUNTING_DT] as DATE),101) AS ACCOUNTING_DT
,convert(varchar(10),cast([T1].[ACTIVITY_DATE] as DATE),101) AS ACTIVITY_DATE
,convert(varchar(10),cast([T2].[SOURCE_DATE] as DATE),101) AS SOURCE_DATE
,[T1].[REQUESTOR_ID]
,[T3].[LAST_NAME]
,[T3].[FIRST_NAME]
,[T1].[OPRID_ENTERED_BY]
,[T4].[LAST_NAME]
,[T4].[FIRST_NAME]
,[T2].[BUYER_ID]
,[T7].[LAST_NAME]
,[T7].[FIRST_NAME]
,[T1].[PROCESS_INSTANCE]
FROM SYSADM_FIN.PS_REQ_HDR T1
LEFT JOIN SYSADM_FIN.PS_REQ_LINE T2
ON T2.REQ_ID=T1.REQ_ID
LEFT JOIN SYSADM_FIN.PS_REQ_LN_DISTRIB T21
ON T21.REQ_ID=T1.REQ_ID AND T21.LINE_NBR=T2.LINE_NBR
LEFT JOIN SYSADM_FIN.PS_PERSONAL_DATA_GGVW T3
ON T3.EMPLID=T1.REQUESTOR_ID
LEFT JOIN SYSADM_FIN.PS_PERSONAL_DATA_GGVW T4
ON T4.EMPLID=T1.OPRID_ENTERED_BY
LEFT JOIN SYSADM_FIN.PS_PERSONAL_DATA_GGVW T6
ON T6.EMPLID=T1.OPRID_APPROVED_BY
LEFT JOIN SYSADM_FIN.PS_PERSONAL_DATA_GGVW T7
ON T7.EMPLID=T2.BUYER_ID
LEFT JOIN SYSADM_FIN.PS_VENDOR T8
ON T8.VENDOR_ID=T2.VENDOR_ID
LEFT JOIN SYSADM_FIN.PS_VENDOR_CNTCT T9
ON T9.VENDOR_ID=T2.VENDOR_ID
LEFT JOIN SYSADM_FIN.PS_PO_LINE_DISTRIB T16
ON T16.REQ_ID=T2.REQ_ID AND T16.REQ_LINE_NBR=T2.LINE_NBR
LEFT JOIN SYSADM_FIN.PS_DEPT_TBL T14
ON T14.DEPTID=T21.DEPTID
LEFT JOIN SYSADM_FIN.PS_GL_ACCOUNT_TBL T15
ON T15.ACCOUNT=T21.ACCOUNT
LEFT JOIN SYSADM_FIN.PS_PO_HDR T17
ON T17.PO_ID=T16.PO_ID
LEFT JOIN SYSADM_FIN.PS_PO_LINE T18
ON T18.PO_ID=T17.PO_ID AND T18.LINE_NBR=T16.LINE_NBR
LEFT JOIN SYSADM_FIN.PS_PERSONAL_DATA_GGVW T19
ON T19.EMPLID=T17.BUYER_ID
LEFT JOIN SYSADM_FIN.PS_PERSONAL_DATA_GGVW T20
ON T20.EMPLID=T17.OPRID_ENTERED_BY
WHERE
(T8.VNDR_NAME_SEQ_NUM = (SELECT MAX(VNDR_NAME_SEQ_NUM) FROM SYSADM_FIN.PS_VENDOR T8_1 WHERE T8_1.VNDR_NAME_SEQ_NUM=T8.VNDR_NAME_SEQ_NUM) OR T8.VENDOR_ID IS NULL)
AND ((T9.EFFDT = (SELECT MAX(EFFDT) FROM SYSADM_FIN.PS_VENDOR_CNTCT T9_1 WHERE T9_1.VENDOR_ID=T9.VENDOR_ID AND T9_1.EFF_STATUS='A')
AND T9.CNTCT_SEQ_NUM = (SELECT MAX(CNTCT_SEQ_NUM) FROM SYSADM_FIN.PS_VENDOR_CNTCT T9_1 WHERE T9_1.VENDOR_ID=T9.VENDOR_ID AND T9_1.EFFDT=T9.EFFDT AND T9_1.EFF_STATUS='A'))
OR T9.EFFDT IS NULL)
AND (T14.EFFDT = (SELECT MAX(EFFDT) FROM SYSADM_FIN.PS_DEPT_TBL T14_1 WHERE T14_1.DEPTID=T14.DEPTID AND T14_1.EFF_STATUS='A') OR T14.EFFDT IS NULL)
AND (T15.EFFDT = (SELECT MAX(EFFDT) FROM SYSADM_FIN.PS_GL_ACCOUNT_TBL T15_1 WHERE T15_1.ACCOUNT=T15.ACCOUNT AND T15_1.EFF_STATUS='A') OR T15.EFFDT IS NULL)
AND
(
(
(LEN('[lccColumnFilter:DateStart]')=0 OR (LEN('[lccColumnFilter:DateStart]')>0 AND [T1].[REQ_DT] >= '[lccColumnFilter:DateStart]'))
AND (LEN('[lccColumnFilter:DateEnd]')=0 OR (LEN('[lccColumnFilter:DateEnd]')>0 AND [T1].[REQ_DT] <= '[lccColumnFilter:DateEnd]'))
)
OR
(
(LEN('[lccColumnFilter:DateStart]')=0 OR (LEN('[lccColumnFilter:DateStart]')>0 AND [T17].[PO_DT] >= '[lccColumnFilter:DateStart]'))
AND (LEN('[lccColumnFilter:DateEnd]')=0 OR (LEN('[lccColumnFilter:DateEnd]')>0 AND [T17].[PO_DT] <= '[lccColumnFilter:DateEnd]'))
)
OR
(
(LEN('[lccColumnFilter:DateStart]')=0 OR (LEN('[lccColumnFilter:DateStart]')>0 AND [T17].[ENTERED_DT] >= '[lccColumnFilter:DateStart]'))
AND (LEN('[lccColumnFilter:DateEnd]')=0 OR (LEN('[lccColumnFilter:DateEnd]')>0 AND [T17].[ENTERED_DT] <= '[lccColumnFilter:DateEnd]'))
)
OR
(
(LEN('[lccColumnFilter:DateStart]')=0 OR (LEN('[lccColumnFilter:DateStart]')>0 AND [T17].[APPROVAL_DT] >= '[lccColumnFilter:DateStart]'))
AND (LEN('[lccColumnFilter:DateEnd]')=0 OR (LEN('[lccColumnFilter:DateEnd]')>0 AND [T17].[APPROVAL_DT] <= '[lccColumnFilter:DateEnd]'))
)
OR
(
(LEN('[lccColumnFilter:DateStart]')=0 OR (LEN('[lccColumnFilter:DateStart]')>0 AND [T1].[ENTERED_DT] >= '[lccColumnFilter:DateStart]'))
AND (LEN('[lccColumnFilter:DateEnd]')=0 OR (LEN('[lccColumnFilter:DateEnd]')>0 AND [T1].[ENTERED_DT] <= '[lccColumnFilter:DateEnd]'))
)
OR
(
(LEN('[lccColumnFilter:DateStart]')=0 OR (LEN('[lccColumnFilter:DateStart]')>0 AND [T1].[APPROVAL_DT] >= '[lccColumnFilter:DateStart]'))
AND (LEN('[lccColumnFilter:DateEnd]')=0 OR (LEN('[lccColumnFilter:DateEnd]')>0 AND [T1].[APPROVAL_DT] <= '[lccColumnFilter:DateEnd]'))
)
OR
(
(LEN('[lccColumnFilter:DateStart]')=0 OR (LEN('[lccColumnFilter:DateStart]')>0 AND [T1].[ACCOUNTING_DT] >= '[lccColumnFilter:DateStart]'))
AND (LEN('[lccColumnFilter:DateEnd]')=0 OR (LEN('[lccColumnFilter:DateEnd]')>0 AND [T1].[ACCOUNTING_DT] <= '[lccColumnFilter:DateEnd]'))
)
OR
(
(LEN('[lccColumnFilter:DateStart]')=0 OR (LEN('[lccColumnFilter:DateStart]')>0 AND [T1].[ACTIVITY_DATE] >= '[lccColumnFilter:DateStart]'))
AND (LEN('[lccColumnFilter:DateEnd]')=0 OR (LEN('[lccColumnFilter:DateEnd]')>0 AND [T1].[ACTIVITY_DATE] <= '[lccColumnFilter:DateEnd]'))
)
OR
(
(LEN('[lccColumnFilter:DateStart]')=0 OR (LEN('[lccColumnFilter:DateStart]')>0 AND [T2].[SOURCE_DATE] >= '[lccColumnFilter:DateStart]'))
AND (LEN('[lccColumnFilter:DateEnd]')=0 OR (LEN('[lccColumnFilter:DateEnd]')>0 AND [T2].[SOURCE_DATE] <= '[lccColumnFilter:DateEnd]'))
)
)

AND (LEN('[lccColumnFilter:[T21].[ACCOUNT]]')=0 OR (LEN('[lccColumnFilter:[T21].[ACCOUNT]]')>0 AND [T21].[ACCOUNT] IN (SELECT * FROM STRING_SPLIT('[lccColumnFilter:[T21].[ACCOUNT]]',','))))
AND (LEN('[lccColumnFilter:[T14].[DESCR]]')=0 OR (LEN('[lccColumnFilter:[T14].[DESCR]]')>0 AND [T14].[DESCR] LIKE '%[lccColumnFilter:[T14].[DESCR]]%'))
AND (LEN('[lccColumnFilter:[T21].[DEPTID]]')=0 OR (LEN('[lccColumnFilter:[T21].[DEPTID]]')>0 AND [T21].[DEPTID] IN (SELECT * FROM STRING_SPLIT('[lccColumnFilter:[T21].[DEPTID]]',','))))
AND (LEN('[lccColumnFilter:[T15].[DESCR]]')=0 OR (LEN('[lccColumnFilter:[T15].[DESCR]]')>0 AND [T15].[DESCR] LIKE '%[lccColumnFilter:[T15].[DESCR]]%'))
AND (LEN('[lccColumnFilter:[T21].[FUND_CODE]]')=0 OR (LEN('[lccColumnFilter:[T21].[FUND_CODE]]')>0 AND [T21].[FUND_CODE] IN (SELECT * FROM STRING_SPLIT('[lccColumnFilter:[T21].[FUND_CODE]]',','))))
AND (LEN('[lccColumnFilter:[T21].[PRODUCT]]')=0 OR (LEN('[lccColumnFilter:[T21].[PRODUCT]]')>0 AND [T21].[PRODUCT] IN (SELECT * FROM STRING_SPLIT('[lccColumnFilter:[T21].[PRODUCT]]',','))))
AND (LEN('[lccColumnFilter:[T21].[CLASS_FLD]]')=0 OR (LEN('[lccColumnFilter:[T21].[CLASS_FLD]]')>0 AND [T21].[CLASS_FLD] IN (SELECT * FROM STRING_SPLIT('[lccColumnFilter:[T21].[CLASS_FLD]]',','))))
AND (
(LEN('[lccColumnFilter:[T1].[REQ_ID]]')=0 OR (LEN('[lccColumnFilter:[T1].[REQ_ID]]')>0 AND [T1].[REQ_ID] IN (SELECT * FROM STRING_SPLIT('[lccColumnFilter:[T1].[REQ_ID]]',',')))
)
OR (LEN('[lccColumnFilter:[T1].[REQ_ID]]')=0 OR ('[lccColumnFilter:[T1].[REQ_ID]]')>0 AND REPLACE(LTRIM(REPLACE([T1].[REQ_ID], '0', ' ')), ' ', '0') ='[lccColumnFilter:[T1].[REQ_ID]]')
)

AND (LEN('[lccColumnFilter:[T1].[REQ_NAME]]')=0 OR (LEN('[lccColumnFilter:[T1].[REQ_NAME]]')>0 AND [T1].[REQ_NAME] LIKE '%[lccColumnFilter:[T1].[REQ_NAME]]%'))
AND (LEN('[lccColumnFilter:[T1].[REQ_DT]]')=0 OR (LEN('[lccColumnFilter:[T1].[REQ_DT]]')>0 AND [T1].[REQ_DT] = '[lccColumnFilter:[T1].[REQ_DT]]'))
AND (LEN('[lccColumnFilter:[T1].[REQUESTOR_ID]]')=0 OR (LEN('[lccColumnFilter:[T1].[REQUESTOR_ID]]')>0 AND [T1].[REQUESTOR_ID] IN (SELECT * FROM STRING_SPLIT('[lccColumnFilter:[T1].[REQUESTOR_ID]]',','))))
AND (LEN('[lccColumnFilter:[T3].[LAST_NAME]]')=0 OR (LEN('[lccColumnFilter:[T3].[LAST_NAME]]')>0 AND [T3].[LAST_NAME] LIKE '%[lccColumnFilter:[T3].[LAST_NAME]]%'))
AND (LEN('[lccColumnFilter:[T3].[FIRST_NAME]]')=0 OR (LEN('[lccColumnFilter:[T3].[FIRST_NAME]]')>0 AND [T3].[FIRST_NAME] LIKE '%[lccColumnFilter:[T3].[FIRST_NAME]]%'))
AND (LEN('[lccColumnFilter:[T1].[OPRID_ENTERED_BY]]')=0 OR (LEN('[lccColumnFilter:[T1].[OPRID_ENTERED_BY]]')>0 AND [T1].[OPRID_ENTERED_BY] IN (SELECT * FROM STRING_SPLIT('[lccColumnFilter:[T1].[OPRID_ENTERED_BY]]',','))))
AND (LEN('[lccColumnFilter:[T4].[LAST_NAME]]')=0 OR (LEN('[lccColumnFilter:[T4].[LAST_NAME]]')>0 AND [T4].[LAST_NAME] LIKE '%[lccColumnFilter:[T4].[LAST_NAME]]%'))
AND (LEN('[lccColumnFilter:[T4].[FIRST_NAME]]')=0 OR (LEN('[lccColumnFilter:[T4].[FIRST_NAME]]')>0 AND [T4].[FIRST_NAME] LIKE '%[lccColumnFilter:[T4].[FIRST_NAME]]%'))
AND (LEN('[lccColumnFilter:[T1].[ENTERED_DT]]')=0 OR (LEN('[lccColumnFilter:[T1].[ENTERED_DT]]')>0 AND [T1].[ENTERED_DT] = '[lccColumnFilter:[T1].[ENTERED_DT]]'))
AND (LEN('[lccColumnFilter:[T1].[APPROVAL_DT]]')=0 OR (LEN('[lccColumnFilter:[T1].[APPROVAL_DT]]')>0 AND [T1].[APPROVAL_DT] = '[lccColumnFilter:[T1].[APPROVAL_DT]]'))
AND (LEN('[lccColumnFilter:[T1].[ACCOUNTING_DT]]')=0 OR (LEN('[lccColumnFilter:[T1].[ACCOUNTING_DT]]')>0 AND [T1].[ACCOUNTING_DT] = '[lccColumnFilter:[T1].[ACCOUNTING_DT]]'))
AND (LEN('[lccColumnFilter:[T1].[ACTIVITY_DATE]]')=0 OR (LEN('[lccColumnFilter:[T1].[ACTIVITY_DATE]]')>0 AND [T1].[ACTIVITY_DATE] = '[lccColumnFilter:[T1].[ACTIVITY_DATE]]'))
AND (LEN('[lccColumnFilter:[T2].[VENDOR_ID]]')=0 OR (LEN('[lccColumnFilter:[T2].[VENDOR_ID]]')>0 AND [T2].[VENDOR_ID] IN (SELECT * FROM STRING_SPLIT('[lccColumnFilter:[T2].[VENDOR_ID]]',','))))
AND (LEN('[lccColumnFilter:[T2].[PRICE_REQ]]')=0 OR (LEN('[lccColumnFilter:[T2].[PRICE_REQ]]')>0 AND [T2].[PRICE_REQ] = '[lccColumnFilter:[T2].[PRICE_REQ]]'))
AND (LEN('[lccColumnFilter:[T2].[DESCR254_MIXED]]')=0 OR (LEN('[lccColumnFilter:[T2].[DESCR254_MIXED]]')>0 AND [T2].[DESCR254_MIXED] LIKE '%[lccColumnFilter:[T2].[DESCR254_MIXED]]%'))
AND (LEN('[lccColumnFilter:[T8].[NAME1]]')=0 OR (LEN('[lccColumnFilter:[T8].[NAME1]]')>0 AND [T8].[NAME1] LIKE '%[lccColumnFilter:[T8].[NAME1]]%'))
AND (
(LEN('[lccColumnFilter:[T16].[PO_ID]]')=0 OR (LEN('[lccColumnFilter:[T16].[PO_ID]]')>0 AND [T16].[PO_ID] IN (SELECT * FROM STRING_SPLIT('[lccColumnFilter:[T16].[PO_ID]]',',')))
)
OR (LEN('[lccColumnFilter:[T16].[PO_ID]]')=0 OR ('[lccColumnFilter:[T16].[PO_ID]]')>0 AND REPLACE(LTRIM(REPLACE([T16].[PO_ID], '0', ' ')), ' ', '0') ='[lccColumnFilter:[T16].[PO_ID]]')
)
ORDER BY [T1].[REQ_ID],[T2].[LINE_NBR]
